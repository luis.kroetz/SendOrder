package com.example.luisgustavo.sendorder;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    TextView campoDeTextoResultado;
    TextView campoDeTextoNome;
    TextView campoDeTextoCobertura;
    TextView campoDeTextoQuantidade;
    TextView campoDeTextoTotal;
    TextView campoDeTextoResumo;

    String strChantilly;
    String strChocolate;
    int temChantilly = 0;
    int temChocolate = 0;
    int valorPedido = 1;

    CheckBox chantillyCheck;
    CheckBox chocolateCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        campoDeTextoResultado = (TextView) findViewById(R.id.resultado);
        campoDeTextoNome = (TextView) findViewById(R.id.nome);
        campoDeTextoCobertura = (TextView) findViewById(R.id.cobertura);
        campoDeTextoQuantidade = (TextView) findViewById(R.id.quantidade);
        campoDeTextoTotal = (TextView) findViewById(R.id.total);
        campoDeTextoResumo = (TextView) findViewById(R.id.resumo);

        chantillyCheck = (CheckBox) findViewById(R.id.chantilly_check);
        chocolateCheck = (CheckBox) findViewById(R.id.chocolate_check);
    }

    public void botaoExibe (View v) {
        EditText inserirTexto = (EditText) findViewById(R.id.inserir_texto);
        TextView campoDeTextoResultado = (TextView) findViewById(R.id.resultado);
        String txt = inserirTexto.getText().toString();

        if (txt.trim().equals("")) {
            String mensagemNome = getResources().getString(R.string.insiraNome);
            Toast.makeText(MainActivity.this, mensagemNome, Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
            alerta();
            caixaTextoInvisivel();
        }

        else {
            String mensagem = getResources().getString(R.string.nome);
            String mensagemQuantidade = getResources().getString(R.string.quantidade);
            String mensagemCobertura = getResources().getString(R.string.coberturas);
            //campoDeTextoNome.setText(mensagem + " " + txt);
            //campoDeTextoQuantidade.setText(mensagemQuantidade + " " + valorPedido);

            String pedidos = "";

            if (chantillyCheck.isChecked()){
                //campoDeTextoCobertura.setText(mensagemCobertura + " " + strChantilly);
                pedidos = "";
                pedidos = mensagemCobertura + " " + strChantilly;

                temChantilly = 1;
                temChocolate = 0;
            }

            if (chocolateCheck.isChecked()){
                //campoDeTextoCobertura.setText(mensagemCobertura + " " + strChocolate);
                pedidos = "";
                pedidos = mensagemCobertura + " " + strChocolate;

                temChantilly = 0;
                temChocolate = 1;
            }

            if (chantillyCheck.isChecked() && chocolateCheck.isChecked()){
                String mensagemE = getResources().getString(R.string.e);
                //campoDeTextoCobertura.setText(mensagemCobertura + " " +strChantilly + " " + mensagemE + " " + strChocolate);
                pedidos = "";
                pedidos = mensagemCobertura + " " +strChantilly + " " + mensagemE + " "+ strChocolate;
                temChantilly = 1;
                temChocolate = 1;
            }

            if (!chantillyCheck.isChecked() && !chocolateCheck.isChecked()){
                String mensagemNaoCobertura = getResources().getString(R.string.naoCobertura);
                //campoDeTextoCobertura.setText(mensagemNaoCobertura);
                pedidos = "";
                pedidos = mensagemNaoCobertura;

                temChantilly = 0;
                temChocolate = 0;
            }

            double compra = 0;
            compra = ((valorPedido*3) + (valorPedido*(temChantilly*2)) + (valorPedido*(temChocolate*1)));

            Locale linguagem = Locale.getDefault();
            String dinheiro = "";
            String idioma = "";
            if (linguagem.getLanguage() == "en"){
                double dolar = 0;
                dolar = compra/3.256;
                String mensagemTotal = getResources().getString(R.string.total);
                dinheiro = "";
                dinheiro = mensagemTotal + " " + format(dolar);
                //campoDeTextoTotal.setText(mensagemTotal + " " + format(dolar));
                idioma = "";
                idioma = " Ordering coffee ";
            }

            else {
                String mensagemTotal = getResources().getString(R.string.total);
                dinheiro = "";
                dinheiro = mensagemTotal + " " + format(compra);
                //campoDeTextoTotal.setText(mensagemTotal + " " + format2(compra));
                idioma = "";
                idioma = " Pedido de café ";
            }
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/plain");
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            share.putExtra(Intent.EXTRA_SUBJECT, idioma);
            share.putExtra(Intent.EXTRA_TEXT, mensagem + " " + txt + "\n" + pedidos + "\n" + mensagemQuantidade + " " + valorPedido + "\n" + dinheiro);
            share.putExtra(Intent.EXTRA_EMAIL, new String[]{"\n" + "geison.camara@digitaldesk.com.br "});
            startActivity(Intent.createChooser(share, "Compartilhar link!"));
        }
    }

    public void chantillyCheck (View v) {
        strChantilly = "Chantilly";
    }

    public void chocolateCheck (View v) {
        strChocolate = "Chocolate";
    }

    public void botaoMenos (View v) {
        String exibeNumero = campoDeTextoResultado.getText().toString();
        valorPedido = Integer.parseInt(exibeNumero);
        valorPedido --;
        if (valorPedido < 1) {
            valorPedido = 1;
            String mensagemMinimo = getResources().getString(R.string.minimo);
            Toast.makeText(MainActivity.this, mensagemMinimo, Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
            alerta();
        }
        campoDeTextoResultado.setText(""+valorPedido);

    }

    public void botaoMais (View v) {
        String exibeNumero = campoDeTextoResultado.getText().toString();
        valorPedido = Integer.parseInt(exibeNumero);
        valorPedido ++;
        if (valorPedido > 100) {
            valorPedido = 100;
            String mensagemMaximo = getResources().getString(R.string.maximo);
            Toast.makeText(MainActivity.this, mensagemMaximo, Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
            alerta();
        }
        campoDeTextoResultado.setText(""+valorPedido);
    }

    public static String format(double dolar) {
        return String.format("%.2f", dolar);
    }

    public static String format2(double compra) {
        return String.format("%.2f", compra);
    }

    public void alerta () {
        Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibra.vibrate(500);
    }

    public void caixaTextoInvisivel () {
        campoDeTextoResumo.setVisibility(View.INVISIBLE);
        campoDeTextoNome.setVisibility(View.INVISIBLE);
        campoDeTextoCobertura.setVisibility(View.INVISIBLE);
        campoDeTextoQuantidade.setVisibility(View.INVISIBLE);
        campoDeTextoTotal.setVisibility(View.INVISIBLE);
    }

}